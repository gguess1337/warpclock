package com.antonok.warpclock

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.os.Handler
import android.provider.AlarmClock
import android.widget.Toast
import androidx.core.app.JobIntentService
import java.time.Instant
import java.time.LocalDate
import java.util.*
import java.util.Calendar.HOUR_OF_DAY
import java.util.Calendar.MINUTE

const val ACTION_SET_WARP_ALARM = "com.antonok.warpclock.action.SET_WARP_ALARM"

const val WARP_HOURS = 8
const val WARP_MINUTES = 0

/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
class AlarmIntentService : JobIntentService() {
    private var mHandler: Handler = Handler()

    fun enqueueWork(context: Context, intent: Intent) {
        enqueueWork(context, AlarmIntentService::class.java, 2, intent)
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onHandleWork(intent: Intent) {
        when (intent.action) {
            ACTION_SET_WARP_ALARM -> {
                handleActionSetAlarm()
            }
        }
    }

    /**
     * Handle action SetAlarm in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionSetAlarm() {
        val currentTime = Calendar.getInstance()

        val hour = currentTime.get(HOUR_OF_DAY)
        val minute = currentTime.get(MINUTE)

        val alarmMinute = (minute + WARP_MINUTES) % 60
        val alarmHour = (hour + WARP_HOURS + (minute + WARP_MINUTES) / 60) % 24

        // Set the new alarm using an Intent
        val alarmIntent = Intent(AlarmClock.ACTION_SET_ALARM).apply {
            putExtra(AlarmClock.EXTRA_HOUR, alarmHour)
            putExtra(AlarmClock.EXTRA_MINUTES, alarmMinute)
            putExtra(AlarmClock.EXTRA_SKIP_UI, true)
        }
        alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(alarmIntent)

        val alarmTime = if(alarmMinute < 10) {
            "$alarmHour:0$alarmMinute"
        } else {
            "$alarmHour:$alarmMinute"
        }

        val toastMsg = if(WARP_MINUTES == 0) {
            "Warp ahead $WARP_HOURS hours to $alarmTime"
        } else {
            "Warp ahead $WARP_HOURS hours and $WARP_MINUTES minutes to $alarmTime"
        }
        // Show a toast.
        mHandler.post {
            Toast.makeText(this, toastMsg, Toast.LENGTH_SHORT).show()
        }

        //Update the widget
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val widgetIds = appWidgetManager.getAppWidgetIds(ComponentName(this, WarpClockWidget::class.java))
        for (appWidgetId in widgetIds) {
            WarpClockWidget.updateAppWidget(applicationContext, appWidgetManager, appWidgetId)
        }
    }

    companion object {
        /**
         * Starts this service to set an alarm with the given parameters. If
         * the service is already performing a task this action will be queued.
         *
         * @see IntentService
         */
        @JvmStatic
        fun startSetAlarm(context: Context) {
            val intent = Intent(context, AlarmIntentService::class.java).apply {
                action = ACTION_SET_WARP_ALARM
            }
            context.startService(intent)
        }
    }
}
