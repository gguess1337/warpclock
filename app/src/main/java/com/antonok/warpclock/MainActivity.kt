package com.antonok.warpclock

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun setAlarm(@Suppress("UNUSED_PARAMETER") view: View) {
        val mIntent = Intent(this, AlarmIntentService::class.java).apply {
            action = ACTION_SET_WARP_ALARM
        }
        AlarmIntentService().enqueueWork(this, mIntent)
    }
}
