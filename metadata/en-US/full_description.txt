Warpclock is a simple Android app that provides a quick shortcut for setting an alarm 8 hours from the current time. The idea is that after a single button press, your head can hit the pillow and you will <i>warp</i> forwards in time with minimal interruption.

<b>How to use</b>

There is a single button inside the app, labeled "Set alarm". Pressing this button will create a new one-shot alarm in your default alarm clock app, configured for 8 hours from the current time. The alarm will automatically delete itself from your alarm app once it has triggered, keeping your list of saved alarms clean.

Once the alarm is created, you'll get two toast notifications. The first notification is from Warpclock, displaying the time that the new alarm should go off. The second notification comes from the system alarm clock, confirming that it was created sucessfully and displaying how far in the future the alarm will occur.


The app also exposes a widget that you can add to your homescreen for even faster access.

In the default launcher, just long-press on your homescreen background and drag the "Warpclock" widget to a location on your homescreen, and drag the edges to resize as desired. Tapping on the widget text will create a new alarm in 8 hours from the current time, just as if you'd pressed the "Set alarm" button in the app. You'll get the same two toast notifications as well.


The alarm is created in your default alarm clock app, so you should use that to configure the default alarm sound and vibration settings. Once an alarm is created, if the trigger time needs to be adjusted, that should also be done in the default alarm clock app.

<b>Current limitations</b>
This app currently does not handle Daylight Savings Time changes, it simply adds 8 hours and rolls over at 24. You'll need to manually fix a couple of alarms per year if you live in a region that still changes the time back and forth.
